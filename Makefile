
# must import key 51852D87348FFC4C before doing this. This makefile doesn't do this.

version=1.4.2
curl=/usr/bin/curl
platform = darwin solaris linux openbsd

vault_$(version)_SHA256SUMS:
	$(curl) -Os https://releases.hashicorp.com/vault/$(version)/vault_$(version)_SHA256SUMS

vault_$(version)_SHA256SUMS.sig:
	$(curl) -Os https://releases.hashicorp.com/vault/$(version)/vault_$(version)_SHA256SUMS.sig

vault_$(version)_%_amd64.zip: vault_$(version)_SHA256SUMS vault_$(version)_SHA256SUMS.sig
	$(curl) -Os https://releases.hashicorp.com/vault/$(version)/$@

%/vault: vault_$(version)_%_amd64.zip
	gpg --verify vault_$(version)_SHA256SUMS.sig vault_$(version)_SHA256SUMS
	grep $*_amd64 vault_$(version)_SHA256SUMS >sum
	shasum -a 256 -c sum
	rm sum
	unzip vault_$(version)_$*_amd64.zip 
	mkdir -p $*
	mv vault $*/

clean:
	rm -rf $(platform)
	rm -rf *.zip
	rm -rf *SHA256SUMS*

