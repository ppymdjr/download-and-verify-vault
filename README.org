* Download and cryptographically verify vault binaries

We will need to work out how to import the key from a key server. It
would probably make sense for me to sign the key myself so I can trust
it.

https://www.hashicorp.com/security

It would be good to use ~signify~ to sign the consequent extracted
binary. This signature indicates the binary having been extracted by,
and delivered from, a *trusted system*. Though, thinking about it,
what's the advantage of *that* signature, as far as a deployed system
is concerned, compared to the Hashicorp signature? Just the fact that
we can verify the /running/ binary I guess. 
